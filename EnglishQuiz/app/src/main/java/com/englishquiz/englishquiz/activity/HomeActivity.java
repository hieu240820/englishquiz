package com.englishquiz.englishquiz.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.englishquiz.englishquiz.R;

public class HomeActivity extends AppCompatActivity {
    private Button btnPlay, btnStructure, btnScore;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        btnPlay = findViewById(R.id.play);
        btnStructure = findViewById(R.id.structure);
        btnScore = findViewById(R.id.score);
        mediaPlayer = MediaPlayer.create(this, R.raw.music_main);
        mediaPlayer.start();
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.stop();
                startActivity(new Intent(HomeActivity.this, PlayActivity.class));
            }
        });
        btnStructure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.stop();
                startActivity(new Intent(HomeActivity.this, StructureActivity.class));
            }
        });
        btnScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.stop();
                startActivity(new Intent(HomeActivity.this, ScoreActivity.class));
            }
        });
    }
}