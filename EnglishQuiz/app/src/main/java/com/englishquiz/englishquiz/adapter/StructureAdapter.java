package com.englishquiz.englishquiz.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.englishquiz.englishquiz.R;
import com.englishquiz.englishquiz.model.StructureModel;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class StructureAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<StructureModel> arrayList;

    public StructureAdapter(Context context, ArrayList<StructureModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.layout_structure, null);
        TextView txtStructure = convertView.findViewById(R.id.txtStructure);
        TextView txtKhangDinh = convertView.findViewById(R.id.txtKhangdinh);
        TextView txtPhuDinh = convertView.findViewById(R.id.txtPhudinh);
        TextView txtNghiVan = convertView.findViewById(R.id.txtNghivan);
        StructureModel structureModel = arrayList.get(position);
        txtStructure.setText(structureModel.getStructe());
        txtKhangDinh.setText(structureModel.getKhangdinh());
        txtPhuDinh.setText(structureModel.getPhudinh());
        txtNghiVan.setText(structureModel.getNghivan());
        return convertView;
    }
}
