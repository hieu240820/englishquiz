package com.englishquiz.englishquiz.model;

public class StructureModel {
    private String structe;
    private String khangdinh;
    private String phudinh;
    private String nghivan;

    public StructureModel(String structe, String khangdinh, String phudinh, String nghivan) {
        this.structe = structe;
        this.khangdinh = khangdinh;
        this.phudinh = phudinh;
        this.nghivan = nghivan;
    }

    public String getStructe() {
        return structe;
    }

    public String getKhangdinh() {
        return khangdinh;
    }

    public String getPhudinh() {
        return phudinh;
    }

    public String getNghivan() {
        return nghivan;
    }
}
