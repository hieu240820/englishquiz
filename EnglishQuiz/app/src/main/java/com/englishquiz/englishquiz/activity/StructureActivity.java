package com.englishquiz.englishquiz.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.englishquiz.englishquiz.R;
import com.englishquiz.englishquiz.adapter.StructureAdapter;
import com.englishquiz.englishquiz.sql.Sqlite;

public class StructureActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_structure);
        toolbar = findViewById(R.id.tb);
        listView = findViewById(R.id.lv);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Structure");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        StructureAdapter structureAdapter = new StructureAdapter(this, new Sqlite(this).getDataStruc());
        listView.setAdapter(structureAdapter);
    }
}