package com.englishquiz.englishquiz.model;

public class ScoreModel {
    private String name;
    private String point;

    public ScoreModel(String name, String point) {
        this.name = name;
        this.point = point;
    }

    public String getName() {
        return name;
    }

    public String getPoint() {
        return point;
    }
}
