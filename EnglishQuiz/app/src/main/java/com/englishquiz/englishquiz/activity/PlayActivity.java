package com.englishquiz.englishquiz.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.englishquiz.englishquiz.R;
import com.englishquiz.englishquiz.model.EnglishModel;
import com.englishquiz.englishquiz.sql.Sqlite;

import java.util.ArrayList;
import java.util.Random;

public class PlayActivity extends AppCompatActivity {
    private TextView txtQues, txtTime, txtHeart, txtNumQues, ttxPoint, txtDetail;
    private RelativeLayout idea;
    private Button btnA, btnB, btnC, btnD;
    private ArrayList<EnglishModel> arrayList;
    private EnglishModel englishModel;
    private int point = 0, heart = 3, numques = 0, time = 20;
    private ImageView img50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        arrayList = new Sqlite(this).getDataQuiz();
        MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.music_main);
        mediaPlayer.start();
        initUi();
        initAction();
        onClick();
        thoiGian();
    }

    private void thoiGian() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (time > 0) {
                    time--;
                    txtTime.setText("00:" + time);

                }
                if (time == 0) {
                    if (heart > 0) {
                        time = 20;
                        initAction();
                    }
                }
                handler.postDelayed(this::run, 1000);
            }
        }, 1000);
    }


    private void onClick() {
        btnA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (heart > 0) {
                    if (englishModel.getAnswer().equalsIgnoreCase("A")) {
                        point += 10;
                        numques++;
                        time = 20;
                        initAction();
                    } else {
                        heart--;
                        numques++;
                        time = 20;
                        initAction();
                    }
                } else {
                    popUp();
                }
            }
        });
        btnB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnC.setVisibility(View.INVISIBLE);
                if (heart > 0) {
                    if (englishModel.getAnswer().equalsIgnoreCase("B")) {
                        point += 10;
                        numques++;
                        time = 20;
                        initAction();
                    } else {
                        heart--;
                        numques++;
                        time = 20;
                        initAction();
                    }
                } else {
                    popUp();
                }
            }
        });
        btnC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (heart > 0) {
                    if (englishModel.getAnswer().equalsIgnoreCase("C")) {
                        point += 10;
                        numques++;
                        time = 20;
                        initAction();
                    } else {
                        heart--;
                        numques++;
                        time = 20;
                        initAction();
                    }
                } else {
                    popUp();
                }
            }
        });
        btnD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (heart > 0) {
                    if (englishModel.getAnswer().equalsIgnoreCase("D")) {
                        point += 10;
                        numques++;
                        time = 20;
                        initAction();
                    } else {
                        heart--;
                        numques++;
                        time = 20;
                        initAction();
                    }
                } else {
                    popUp();
                }
            }
        });
        idea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetail();
            }
        });

        img50.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                truDapan();
            }
        });

    }

    private void truDapan() {
        switch (englishModel.getAnswer()) {
            case "A":
                btnB.setVisibility(View.INVISIBLE);
                btnD.setVisibility(View.INVISIBLE);
                break;
            case "B":
                btnA.setVisibility(View.INVISIBLE);
                btnC.setVisibility(View.INVISIBLE);
                break;
            case "C":
                btnA.setVisibility(View.INVISIBLE);
                btnD.setVisibility(View.INVISIBLE);
                break;
            case "D":
                btnA.setVisibility(View.INVISIBLE);
                btnB.setVisibility(View.INVISIBLE);
                break;
        }
    }

    private void showDetail() {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.layout_show_detail);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
        TextView txtDetail = dialog.findViewById(R.id.txtDetail);
        txtDetail.setText(englishModel.getDetail());

    }

    private void popUp() {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.layout_popup);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        EditText editText = dialog.findViewById(R.id.edt);
        Button btnSave = dialog.findViewById(R.id.btnSave);
        Button btnConti = dialog.findViewById(R.id.btnContinue);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editText.getText().toString().trim();
                if (name.length() > 0) {
                    new Sqlite(PlayActivity.this).addPoint(name, String.valueOf(point));
                    startActivity(new Intent(PlayActivity.this, ScoreActivity.class));
                } else {
                    Toast.makeText(PlayActivity.this, "Enter your name", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnConti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                point = 0;
                heart = 3;
                numques = 0;
                initAction();
                dialog.dismiss();
            }
        });
    }


    private void initAction() {
        Random local = new Random();
        int ques = local.nextInt(arrayList.size() - 1) + 1;
        englishModel = arrayList.get(ques);
        txtQues.setText(englishModel.getQues());
        btnA.setText(englishModel.getA());
        btnB.setText(englishModel.getB());
        btnD.setText(englishModel.getD());
        btnC.setText(englishModel.getC());
        txtHeart.setText(heart + "/3");
        txtNumQues.setText(numques + 1 + "/" + arrayList.size());
        ttxPoint.setText(point + "");
        btnA.setVisibility(View.VISIBLE);
        btnB.setVisibility(View.VISIBLE);
        btnC.setVisibility(View.VISIBLE);
        btnD.setVisibility(View.VISIBLE);
    }

    private void initUi() {
        txtQues = findViewById(R.id.txtques);
        btnA = findViewById(R.id.A);
        btnB = findViewById(R.id.B);
        btnC = findViewById(R.id.C);
        btnD = findViewById(R.id.D);
        idea = findViewById(R.id.idea);
        txtTime = findViewById(R.id.time);
        txtHeart = findViewById(R.id.heart);
        txtNumQues = findViewById(R.id.numques);
        ttxPoint = findViewById(R.id.point);
        img50 = findViewById(R.id.img50);
    }
}