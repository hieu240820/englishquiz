package com.englishquiz.englishquiz.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.englishquiz.englishquiz.model.EnglishModel;
import com.englishquiz.englishquiz.model.ScoreModel;
import com.englishquiz.englishquiz.model.StructureModel;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

public class Sqlite extends SQLiteAssetHelper {
    private SQLiteDatabase sqLiteDatabase;
    public Sqlite(Context context) {
        super(context, "English.db",  null, 1);
        sqLiteDatabase = getWritableDatabase();
    }
    public ArrayList<EnglishModel> getDataQuiz(){
        ArrayList<EnglishModel> arrayList = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM EnglishQuiz", null);
        while (cursor.moveToNext()){
            arrayList.add(new EnglishModel(cursor.getString(1), cursor.getString(2), cursor.getString(3),
                    cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7)));
        }
        return arrayList;
    }
    public ArrayList<StructureModel> getDataStruc(){
        ArrayList<StructureModel> arrayList = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM Structure", null);
        while(cursor.moveToNext()){
            arrayList.add(new StructureModel(cursor.getString(1), cursor.getString(2),
                    cursor.getString(3), cursor.getString(4) ));
        }
        return  arrayList;
    }
    public ArrayList<ScoreModel> getDataScore(){
        ArrayList<ScoreModel> arrayList = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM Score", null);
        while (cursor.moveToNext()){
            arrayList.add(new ScoreModel(cursor.getString(1), cursor.getString(2)));
        }
        return  arrayList;
    }
    public void addPoint(String name, String point){
        ContentValues contentValues = new ContentValues();
        contentValues.put("NAME", name);
        contentValues.put("POINT", point);
        sqLiteDatabase.insert("Score", null, contentValues);
    }


}
