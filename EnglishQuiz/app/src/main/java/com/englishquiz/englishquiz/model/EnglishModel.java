package com.englishquiz.englishquiz.model;

public class EnglishModel {
    private String ques;
    private String a, b, c, d;
    private String answer;
    private String detail;

    public EnglishModel(String ques, String a, String b, String c, String d, String answer, String detail) {
        this.ques = ques;
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.answer = answer;
        this.detail = detail;
    }

    public String getQues() {
        return ques;
    }

    public String getA() {
        return a;
    }

    public String getB() {
        return b;
    }

    public String getC() {
        return c;
    }

    public String getD() {
        return d;
    }

    public String getAnswer() {
        return answer;
    }

    public String getDetail() {
        return detail;
    }
}
