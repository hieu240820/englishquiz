package com.englishquiz.englishquiz.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.englishquiz.englishquiz.R;
import com.englishquiz.englishquiz.model.ScoreModel;

import java.util.ArrayList;

public class ScoreAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<ScoreModel> arrayList;

    public ScoreAdapter(Context context, ArrayList<ScoreModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.layout_score, null);
        TextView txtName = convertView.findViewById(R.id.txtName);
        TextView txtPoint = convertView.findViewById(R.id.txtPoint);
        ScoreModel scoreModel = arrayList.get(position);
        txtName.setText(scoreModel.getName());
        txtPoint.setText(scoreModel.getPoint());

        return convertView;
    }
}
