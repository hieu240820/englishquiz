package com.englishquiz.englishquiz.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.englishquiz.englishquiz.R;
import com.englishquiz.englishquiz.adapter.ScoreAdapter;
import com.englishquiz.englishquiz.sql.Sqlite;

public class ScoreActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        toolbar = findViewById(R.id.toolbar2);
        listView = findViewById(R.id.lv2);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Score");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ScoreAdapter scoreAdapter = new ScoreAdapter(this, new Sqlite(this).getDataScore());
        listView.setAdapter(scoreAdapter);
    }
}